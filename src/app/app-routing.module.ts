import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { HomeComponent } from './modules/auth/components/home/home.component';

const routes: Routes = [
  {
    path: "",
    loadChildren: "./modules/auth/auth.module#AuthModule"
  },
  {
    path: "home",
    component: HomeComponent,
    children: [
      // { path: "", redirectTo: "account", pathMatch: "full" },
      {
        path: "account",
        loadChildren: "./modules/accounts/accounts.module#AccountsModule"
      },
      {
        path: "payment",
        loadChildren: "./modules/payments/payments.module#PaymentsModule"
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
