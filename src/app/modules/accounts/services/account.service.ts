import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AccountList } from "../models/accounts.models";
import { environment } from "src/environments/environment";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { map } from 'rxjs/operators';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
  providedIn: "root"
})
export class AccountService {
  _api_URL = environment.baseAPI;

  constructor(
    private httpService: HttpClientService,
    private http: HttpClient
  ) { }

  getAccount(): Observable<AccountList[]> {
    return this.httpService.get(`${this._api_URL}account`);
  }

  postAccount(data) {
  //  return this.http.post(`${this._api_URL}account/add`, data).subscribe((res) => {
  //     console.log(res);
  //   });
  return this.http.post(`${this._api_URL}account/add`, data);
  }

}
