import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { GridModule } from "@progress/kendo-angular-grid";
import { AccountsRoutingModule } from "./accounts-routing.module";
import { AddAccountComponent } from "./components/add-account/add-account.component";
import { DetailAccountComponent } from "./components/detail-account/detail-account.component";
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AddAccountComponent, DetailAccountComponent],
  imports: [
    AccountsRoutingModule,
    CommonModule,
    FormsModule,
    GridModule,
    HttpClientModule,
    ReactiveFormsModule
  ]
})
export class AccountsModule {}
