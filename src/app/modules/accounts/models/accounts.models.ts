export interface AccountList{
    id: string,
    accountNumber: number,
    accountHolderName: string,
    accountHolderPhoneNumber: number,
    accountDescription: string
}

export interface ColumnSetting {
    field: string;
    title: string;
    format?: string;
    type: 'text' | 'numeric' | 'boolean' | 'date';
  }