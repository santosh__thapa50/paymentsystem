import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { GridModule } from "@progress/kendo-angular-grid";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { PaymentsRoutingModule } from "./payments-routing.module";
import { DetailPaymentComponent } from "./components/detail-payment/detail-payment.component";
import { AddPaymentComponent } from "./components/add-payment/add-payment.component";

@NgModule({
  declarations: [DetailPaymentComponent, AddPaymentComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    GridModule,
    PaymentsRoutingModule
  ]
})
export class PaymentsModule {}
