import { Component, OnInit } from "@angular/core";
import { PaymentService } from "../../services/payment.service";
import { Payment } from "../../models/payments.model";
import { GridDataResult, PageChangeEvent } from "@progress/kendo-angular-grid";

@Component({
  selector: "app-detail-payment",
  templateUrl: "./detail-payment.component.html",
  styleUrls: ["./detail-payment.component.scss"]
})
export class DetailPaymentComponent implements OnInit {
  paymentsList: Payment[] = [];
  paymenetDetails: Payment = {
    sourceAccountNumber: 0,
    destinationAccountNumber: 0,
    amount: 0,
    currencyCode: null,
    paymentDescription: null
  };
  public pageSize = 10;
  public skip = 0;
  public gridView: GridDataResult;

  constructor(private paymentService: PaymentService) {}

  ngOnInit() {
    this.paymentService.getPayments().subscribe(res => {
      this.paymentsList = res;
    });
  }

  loadItems(): void {
    this.gridView = {
      data: this.paymentsList.slice(this.skip, this.skip + this.pageSize),
      total: this.paymentsList.length
    };
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
  }

  details(dataItems): void {
    this.paymenetDetails = dataItems;
  }
}
