import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { PaymentService } from "../../services/payment.service";
import { CustomValidationService } from "src/app/services/custom-validation.service";
import { AccountList } from 'src/app/modules/accounts/models/accounts.models';

@Component({
  selector: "app-add-payment",
  templateUrl: "./add-payment.component.html",
  styleUrls: ["./add-payment.component.scss"]
})
export class AddPaymentComponent implements OnInit {
  addPaymentForm: FormGroup;
  accountList: AccountList[] = [];
  currency: any;
  submitted: boolean = false;
  constructor(
    private _fb: FormBuilder,
    private paymentService: PaymentService,
    private router: Router,
    private customValidation: CustomValidationService
  ) {}

  ngOnInit() {
    this.addPaymentForm = this._fb.group({
      sourceAccountNumber: [
        "",
        this.customValidation.checkLimit(100000, 999999)
      ],
      destinationAccountNumber: [
        "",
        this.customValidation.checkLimit(100000, 999999)
      ],
      amount: [""],
      currencyCode: [""],
      paymentDescription: [""]
    });

    this.paymentService.getCurrency().subscribe(res => {
      this.currency = res;
    });

    this.paymentService.getAccount().subscribe(res => {
      this.accountList = res;
    });
  }

  get f() {
    return this.addPaymentForm.controls;
  }

  save(): void {
    this.submitted = true;
    if (this.addPaymentForm.valid) {
      this.paymentService
        .postPayment(this.addPaymentForm.value)
        .subscribe(res => {
          this.router.navigate(["/home/payment"]);
        });
    } else {
    }
  }

  public cancel(): void {
    this.addPaymentForm.reset();
    this.router.navigate(["/home/payment"]);
  }
}
