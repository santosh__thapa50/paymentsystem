import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { Payment } from '../models/payments.model';
import { AccountList } from '../../accounts/models/accounts.models';
import { HttpClientService } from 'src/app/services/http-client/http-client.service';

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  _api_URL = environment.baseAPI;

  constructor(
    private httpService: HttpClientService,
    private http: HttpClient
  ) {}

  getAccount(): Observable<AccountList[]> {
    return this.httpService.get(`${this._api_URL}account`);
  }
  getPayments() {
    return this.httpService.get(`${this._api_URL}payment`);
  }

  postPayment(data): Observable<Payment> {
    return this.http.post<Payment>(`${this._api_URL}payment/add`, data);
  }

  getCurrency() {
    return this.http.get(`${this._api_URL}currency`);
  }
}
